﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service.Model
{
    public class GiteeConfig
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        public string Repo { get; set; }
        /// <summary>
        /// 分支
        /// </summary>
        public string Branch { get; set; }
    }

}
