﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service.Model.ViewModel
{
    public class ResponseModel<T>
    {
        /// <summary>
        /// 信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 总数
        /// </summary>
        public int Total { get; set; }
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; } = false;
        /// <summary>
        /// 错误码
        /// </summary>
        public string ErrorCode { get; set; }
        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }
    }
    /// <summary>
    /// 返回数据集合模型
    /// </summary>
    public class ResponseListModel<T> : ResponseModel<List<T>>
    {

    }
    /// <summary>
    /// 返回数据模型
    /// </summary>
    public class ResponseModel : ResponseModel<object>
    {
        public ResponseModel(bool success, string msg)
        {
            this.Success = success;
            this.Message = msg;
        }
        public ResponseModel()
        {

        }
    }
}
