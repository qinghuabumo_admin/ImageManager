﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service.Model.RequestModel
{
    public class CreateFileModel
    {
        public string access_token { get; set; }
        public string content { get; set; }
        /// <summary>
        /// 提交信息
        /// </summary>
        public string message { get; set; }
    }
}
