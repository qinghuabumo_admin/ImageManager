﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service.Model.RequestModel
{
    public class UpdateFileModel
    {
        /// <summary>
        /// 图片base64
        /// </summary>
        public string Base64Str { get; set; }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
    }
}
