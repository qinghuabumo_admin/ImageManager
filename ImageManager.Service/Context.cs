﻿using ImageManager.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service
{
    public class Context
    {
        /// <summary>
        /// GiteeConfig
        /// </summary>
        public static GiteeConfig GiteeSetting { get; set; } = new GiteeConfig();
    }
}
