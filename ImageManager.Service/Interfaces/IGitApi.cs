﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service.Interfaces
{
    public interface IGitApi
    {
        string GetTreeData();
        string CreateFile(string imageBase64, string fileName, string message = "添加一个文件");
    }
}
