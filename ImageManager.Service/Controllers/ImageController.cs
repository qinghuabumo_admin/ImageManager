﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ImageManager.Service.Interfaces;
using ImageManager.Service.Model.RequestModel;
using ImageManager.Service.Model.ResponseModel;
using ImageManager.Service.Model.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ImageManager.Service.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public string GetTreeData()
        {
            IGitApi api = new GiteeApi();
            return api.GetTreeData();
        }
        [HttpPost]
        public ResponseModel UpdateFile(UpdateFileModel model)
        {
            ResponseModel responseModel = new ResponseModel();
            if(model == null || string.IsNullOrEmpty(model.Base64Str) || string.IsNullOrEmpty(model.FileName))
            {
                responseModel.Success = false;
                responseModel.Message = "参数异常";
                return responseModel;
            }
            IGitApi api = new GiteeApi();
            var extension = Path.GetExtension(model.FileName);
            string fileName = Guid.NewGuid().ToString() + extension;
            string result = api.CreateFile(model.Base64Str, fileName);
            if (string.IsNullOrEmpty(result))
            {
                responseModel.Success = false;
                responseModel.Message = "上传异常";
            }
            else
            {
                var resultModel = JsonConvert.DeserializeObject<GiteeCreateResponseModel>(result);
                responseModel.Success = true;
                responseModel.Data = resultModel.content.download_url;
            }
            return responseModel;
        }
        public string GetBase64FromImage(string imagefile)
        {
            return  Convert.ToBase64String(System.IO.File.ReadAllBytes(imagefile));
        }

    }
}
