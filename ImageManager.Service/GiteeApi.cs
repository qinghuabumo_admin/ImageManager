﻿using ImageManager.Service.Common;
using ImageManager.Service.Interfaces;
using ImageManager.Service.Model;
using ImageManager.Service.Model.RequestModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageManager.Service
{
    public class GiteeApi : IGitApi
    {
        public string Url = "https://gitee.com/api/v5/";
        public string CreateFile(string imageBase64,string fileName,string message = "添加一个文件")
        {
            string url = $"{Url}repos/{Context.GiteeSetting.Owner}/{Context.GiteeSetting.Repo}/contents/{fileName}";
            CreateFileModel createFileModel = new CreateFileModel();
            createFileModel.access_token = Context.GiteeSetting.AccessToken;
            createFileModel.message = message;
            createFileModel.content = imageBase64;
            HttpHelper http = new HttpHelper();
            http.SetContentType("application/json;charset=UTF-8");
            string result = http.HttpPost(url,JsonConvert.SerializeObject(createFileModel));
            return result;
        }

        public string GetTreeData()
        {
            string url = $"{Url}repos/{Context.GiteeSetting.Owner}/{Context.GiteeSetting.Repo}/git/trees/{Context.GiteeSetting.Branch}?access_token={Context.GiteeSetting.AccessToken}";
            HttpHelper http = new HttpHelper();
            string result = http.HttpGet(url);
            return result;
        }
    }
}
